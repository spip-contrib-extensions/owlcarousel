<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// O
	'owlcarousel_description' => 'OWL Carousel un plugin jQuery, support du Touch, mode responsive, lazy-load et plus.

-* [Owl Carousel - Demo -> https://owlcarousel2.github.io/OwlCarousel2/demos/demos.html]
-* [Owl Carousel - GitHub -> https://owlcarousel2.github.io/OwlCarousel2/]',
	'owlcarousel_nom' => 'Owl Carousel',
	'owlcarousel_slogan' => 'Carousel responsive avec support du multi-touch.',

);
