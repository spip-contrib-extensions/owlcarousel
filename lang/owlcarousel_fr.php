<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// O
	'owlcarousel_titre' => 'Owl Carousel',

	// C
	'cfg_label_css'	=> 'Insertion des styles',
	'cfg_explication_css'	=> 'Les styles seront insérés dans l\'entête de toutes les pages',
	'cfg_titre_page_configurer_owlcarousel'	=> 'OwlCarousel',
	'cfg_label_header_prive'	=> 'Insertion dans l\'espace privé de spip',
	'cfg_explication_header_prive'	=> 'Les styles et javascript seront insérés dans l\'entête de l\'espace privé de spip',
	'cfg_titre_parametrages' => 'Configurer OwlCarousel',

	// P
	'prec'=>'précédent',
	'suiv'=>'suivant',
	// T
	'titre_menu' => 'Owl Carousel',
	'titre_page_configurer_owlcarousel' => 'configurer_owlcarousel',
);

